export const square = x => x * x;

export const quickSort = arr => {
  if (arr.length < 2) {
    return arr;
  }

  const pivot = arr[arr.length - 1];
  const arr1 = [];
  const arr2 = [];
  for (let i = 0; i < arr.length - 1; i += 1) {
    if (arr[i] < pivot) {
      arr1.push(arr[i]);
    } else {
      arr2.push(arr[i]);
    }
  }

  return quickSort(arr1).concat(pivot, quickSort(arr2));
};

export const halve = n => n / 2;

export const makeNoise = () => {
  console.log('Pling!');
};

export const power = (base, exponent) => {
  let result = 1;
  for (let count = 0; count < exponent; count += 1) {
    result *= base;
  }
  return result;
};

// export const hummus = factor => {
//   const ingredient = (amount, unit, name) => {
//     const ingredientAmount = amount * factor;
//     if (ingredientAmount > 1) {
//       unit += 's';
//     }
//     console.log(`${ingredientAmount} ${unit} ${name}`);
//   };
// };

export const horn = () => {
  console.log('Toot');
};

export const future = () => 'You will never have flying cars';

export const minus = (a, b) => {
  if (b === undefined) {
    return -a;
  }
  return a - b;
};

export const powerFunction = (base, exponent = 2) => {
  let result = 1;
  for (let count = 0; count < exponent; count += 1) {
    result *= base;
  }
  return result;
};

export const wrapValue = n => {
  const local = n;
  return () => local;
};

export const multiplier = factor => number => number * factor;

export const powerRecursive = (base, exponent) => {
  if (exponent === 0) {
    return 1;
  }
  return base * power(base, exponent - 1);
};

export const printFarmInventory = (cows, chickens) => {
  let cowString = String(cows);
  while (cowString.length < 3) {
    cowString = `0${cowString}`;
  }
  console.log(`${cowString} Cows`);
  let chickenString = String(chickens);
  while (chickenString.length < 3) {
    chickenString = `0${chickenString}`;
  }
  console.log(`${chickenString} Chickens`);
};

export const printZeroPaddedWithLabel = (number, label) => {
  let numberString = String(number);
  while (numberString.length < 3) {
    numberString = `0${numberString}`;
  }
  console.log(`${numberString} ${label}`);
};

export const printFarmInventorys = (cows, chickens, pigs) => {
  printZeroPaddedWithLabel(cows, 'Cows');
  printZeroPaddedWithLabel(chickens, 'Chickens');
  printZeroPaddedWithLabel(pigs, 'Pigs');
};

export const zeroPad = (number, width) => {
  let string = String(number);
  while (string.length < width) {
    string = `0${string}`;
  }
  return string;
};

export const printFarmInventory3 = (cows, chickens, pigs) => {
  console.log(`${zeroPad(cows, 3)} Cows`);
  console.log(`${zeroPad(chickens, 3)} Chickens`);
  console.log(`${zeroPad(pigs, 3)} Pigs`);
};

export const phi = table =>
  table[3] * table[0] -
  (table[2] * table[1]) /
    Math.sqrt(
      (table[2] + table[3]) *
        (table[0] + table[1]) *
        (table[1] + table[3]) *
        (table[0] + table[2]),
    );
export const tableFor = (event, journal) => {
  const table = [0, 0, 0, 0];
  for (let i = 0; i < journal.length; i += 1) {
    const entry = journal[i];
    i = 0;
    if (entry.events.includes(event)) i += 1;
    if (entry.squirrel) i += 2;
    table[i] += 1;
  }
  return table;
};

export const journalEvents = journal => {
  const events = [];
  for (const entry of journal) {
    for (const event of entry.events) {
      if (!events.includes(event)) {
        events.push(event);
      }
    }
  }
  return events;
};

export const remove = (array, index) =>
  array.slice(0, index).concat(array.slice(index + 1));

export const max = (...numbers) => {
  let result = -Infinity;
  for (const number of numbers) {
    if (number > result) result = number;
  }
  return result;
};

export const randomPointOnCircle = radius => {
  const angle = Math.random() * 2 * Math.PI;
  return { x: radius * Math.cos(angle), y: radius * Math.sin(angle) };
};
