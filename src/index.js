import '@babel/polyfill';

import { range } from 'rxjs';
import { filter, map, reduce } from 'rxjs/operators';
import { assert } from 'tcomb';

import {
  future,
  halve,
  horn,
  journalEvents,
  makeNoise,
  max,
  minus,
  multiplier,
  phi,
  power,
  powerFunction,
  powerRecursive,
  printFarmInventory,
  printFarmInventory3,
  printFarmInventorys,
  printZeroPaddedWithLabel,
  quickSort,
  randomPointOnCircle,
  remove,
  square,
  wrapValue,
} from './programs';

range(1, 10)
  .pipe(
    filter(x => x % 2 === 0),
    map(x => x * x),
    reduce((x, y) => x + y),
  )
  .subscribe(x => assert(x === 220));
console.log('hai');
console.log(square(9));
console.log(quickSort([9, 2, 5, 6, 4, 3, 7, 10, 1, 8]));
console.log(power(2, 3));
console.log('The future says:', future());
console.log(horn());
console.log(minus(10));
console.log(minus(10, 5));
console.log(powerFunction(4));
console.log(powerFunction(2, 6));
const wrap1 = wrapValue(1);
const wrap2 = wrapValue(2);
console.log(wrap1());
console.log(wrap2());
console.log(halve(50));
console.log(makeNoise());
const twice = multiplier(2);
console.log(twice(5));
console.log(powerRecursive(2, 3));
console.log(printFarmInventory(7, 11));
console.log(printZeroPaddedWithLabel(7, 11));
console.log(printFarmInventorys(7, 11, 3));
console.log(printFarmInventory3(7, 16, 3));

const listOfNumbers = [2, 3, 5, 7, 11];
console.log(listOfNumbers[2]);
console.log(listOfNumbers[0]);
console.log(listOfNumbers[2 - 1]);

const doh = 'Doh';
console.log(typeof doh.toUpperCase);
console.log(doh.toUpperCase());

const sequence = [1, 2, 3];
sequence.push(4);
sequence.push(5);
console.log(sequence);
console.log(sequence.pop());
console.log(sequence);

const day1 = {
  squirrel: false,
  events: ['work', 'touched tree', 'pizza', 'running'],
};
console.log(day1.squirrel);
console.log(day1.wolf);
day1.wolf = false;
console.log(day1.wolf);

const anObject = {
  left: 1,
  right: 2,
};
console.log(anObject.left);
delete anObject.left;
console.log(anObject.left);
console.log('left' in anObject);
console.log('right' in anObject);

const objectA = {
  a: 1,
  b: 2,
};
Object.assign(objectA, { b: 3, c: 4 });
console.log(objectA);

const object1 = {
  value: 10,
};
const object2 = object1;
const object3 = {
  value: 10,
};
console.log(object1 === object2);
console.log(object1 === object3);
object1.value = 15;
console.log(object2.value);
console.log(object3.value);

console.log(phi([76, 9, 4, 1]));

const journal = [
  {
    events: ['work', 'touched tree', 'pizza', 'running', 'television'],
    squirrel: false,
  },
  {
    events: [
      'work',
      'ice cream',
      'cauliflower',
      'lasagna',
      'touched tree',
      'brushed teeth',
    ],
    squirrel: false,
  },
  {
    events: ['weekend', 'cycling', 'break', 'peanuts', 'beer'],
    squirrel: true,
  },
];
console.log(journalEvents(journal));
console.log(remove(['a', 'b', 'c', 'd', 'e'], 2));
console.log(max(4, 1, 9, -2));
console.log(randomPointOnCircle(2));
